<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" version="2.0">
	
	<xsl:output doctype-system="res.dtd" indent="yes" method="xml" encoding="UTF-8"/>
	<xsl:strip-space elements="*"/>
	<!-- 1 ère règle : Liste de tout les présidents de la république -->
	<xsl:template match="/">
    	<liste-présidents>
        	<xsl:apply-templates select ="déplacements/liste-personnes/personne[./fonction/@type = 'Président de la République']"></xsl:apply-templates>
        </liste-présidents>
	</xsl:template>

	<!-- 2 eme règle : Nous affichon les présidents en affichant leurs noms et nous selectionnons  les  pays africains  pour chaque président-->

	<xsl:template match="personne">
     	<président>
            <xsl:attribute name="nom" select="@nom" />
         	<xsl:apply-templates select="/déplacements/liste-pays/pays[ ./encompassed/@continent = 'africa']">
			<xsl:with-param name="xml-id" select="fonction/@xml:id"/>
         	</xsl:apply-templates>
      	</président>
	</xsl:template>

	<!-- 3 eme règle :  Nous affichons les pays accompagnées par leurs nom , le durée totale passée dans  ces pays par chaque président ainsi que si le pays est francophone ou non-->

	<xsl:template match="pays">
	       <xsl:param name="xml-id"/>
    	<pays >
		    <xsl:attribute name="nom" select="@nom"></xsl:attribute>
			<xsl:variable name="duree" select="sum(/déplacements/liste-visites/visite[./@personne = $xml-id and  @pays = current()/@xml:id]/days-from-duration(xs:date(@fin) - xs:date(@debut))) + count(/déplacements/liste-visites/visite[./@personne =$xml-id and  @pays = current()/@xml:id]) "/>
	<!-- le choose sert à mettre une condition sur la durée si elle est supérieure à 0 nous l'affichons sou la forme P(duree)D sinon nous affichons juste la durée  -->	
			<xsl:choose>
                <xsl:when test="$duree > 0">
                <xsl:attribute name="durée" select="concat('P',$duree,'D')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="durée" select="0"/>

                </xsl:otherwise>
             </xsl:choose>
			 <!--Nous testons le pourcentage de la langue Française dans chaque pays pour attribuer le bon attribut Francophone -->
			<xsl:if test="./language[contains(.,'French') and  (@percentage >=30  or (not(@percentage)))]"> 
            <xsl:attribute name="francophone" select="if( ./language[(not(@percentage) ) and contains(.,'French')] ) then 'Officiel' else 'En-partie'   " ></xsl:attribute> 
            </xsl:if>
	    </pays>
    </xsl:template> 
	
</xsl:transform>