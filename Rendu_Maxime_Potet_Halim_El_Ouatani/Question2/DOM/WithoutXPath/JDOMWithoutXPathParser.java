import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;


public class JDOMWithoutXPathParser {

    public static void main(String[] args) {
        try {
            File inputFile = new File("tp.xml");
            SAXBuilder saxBuilder = new SAXBuilder();
            Document document = saxBuilder.build(inputFile);
            Element deplacementsElt = document.getRootElement();
            Element listePaysElt = deplacementsElt.getChild("liste-pays");
            Element listeVisitesElt = deplacementsElt.getChild("liste-visites");
            Element listePersonnesElt = deplacementsElt.getChild("liste-personnes");

            ArrayList<String> paysAfricains = new ArrayList<>(); // Liste de nom_pays_africain
            HashMap<String, String> presidents = new HashMap<>(); // Tableau associatif [Clé: nom_président, Val: fonction_id]
            HashMap<String, Integer> visitesPaysAfricains = new HashMap<>(); // Tableau associatif [Clé: nom_pays_africain;fonction_id, Val: nombre_visites]
            HashMap<String, String> paysFrLanguage = new HashMap<>(); // Tableau associatif [Clé: nom_pays_africain, Val: texte_francophone]

            /*
                Pour chaque pays,
                Si il est africain,
                On l'enregistre dans paysAfricains
                Et on regarde si ce pays parle français en-partie, officiellement, ou non
            */
            listePaysElt.getChildren().forEach((Element paysElt) -> {
                try{
                    String pays = paysElt.getAttributeValue("nom");
                    if(paysElt.getChild("encompassed").getAttributeValue("continent").equals("africa")){
                        paysAfricains.add(pays);
                        paysElt.getChildren("language").forEach((Element langage) -> {
                            String text = langage.getText();
                            if(text.equals("French")){
                                double percentage;
                                try{
                                    percentage = Double.parseDouble(langage.getAttributeValue("percentage"));
                                }
                                catch (Exception e){
                                    percentage = -1.0;
                                }
                                if(percentage >= 30){
                                    paysFrLanguage.put(pays, "En-partie");
                                }
                                else if(percentage == -1.0){
                                    paysFrLanguage.put(pays, "Officiel");
                                }
                                else{
                                    paysFrLanguage.put(pays, "");
                                }
                            }
                        });
                    }
                } catch (Exception ignored) {}
            });

            /*
                Pour chaque personne,
                Si elle a la fonction "Président de la République",
                On enregistre son nom et l'id de sa fonction dans presidents
            */
            listePersonnesElt.getChildren().forEach((Element personneElt) -> {
                try{
                    if(personneElt.getChild("fonction").getAttribute("type").getValue().equals("Président de la République"))
                        presidents.put(personneElt.getAttributeValue("nom"), personneElt.getChild("fonction").getAttributes().get(1).getValue());
                } catch (Exception ignored) {}
            });

            /*
                Pour chaque visite,
                Si elle s'effectue dans un pays africain,
                On enregistre le pays, l'id de la fonction du visiteur et la durée totale de visite dans visitesPaysAfricains
            */
            listeVisitesElt.getChildren().forEach((Element visiteElt) -> {
                try{
                    String pays = visiteElt.getAttributeValue("pays");
                    if(paysAfricains.contains(pays)){
                        String personneFull = visiteElt.getAttributeValue("personne");
                        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                        LocalDate dateDebut = LocalDate.parse(visiteElt.getAttributeValue("debut"), df);
                        LocalDate dateFin = LocalDate.parse(visiteElt.getAttributeValue("fin"), df);
                        int duree = (int) ChronoUnit.DAYS.between(dateDebut, dateFin) + 1;
                        if(visitesPaysAfricains.containsKey(pays + ";" + personneFull)){
                            int lastduree = visitesPaysAfricains.get(pays + ";" + personneFull);
                            visitesPaysAfricains.put(pays + ";" + personneFull, lastduree + duree);
                        }
                        else{
                            visitesPaysAfricains.put(pays + ";" + personneFull, duree);
                        }
                    }
                } catch (Exception ignored) {}
            });

            /*
                Affichage du contenu du fichier de sortie
            */
            System.out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<!DOCTYPE liste-présidents\n" +
                    "  SYSTEM \"res.dtd\">");
            System.out.println("<liste-présidents>");
            presidents.forEach((presidentName, presidentFull) -> {
                System.out.println("    <président nom=\"" + presidentName + "\">");
                paysAfricains.forEach((nomPays) -> {
                    int duree = 0;
                    String francophoneString = "" + paysFrLanguage.get(nomPays);
                    String attrFrancophone = "";
                    if(!francophoneString.isEmpty() && !francophoneString.equals("null")) attrFrancophone = "franchophone=\"" + francophoneString + "\"";
                    if(visitesPaysAfricains.containsKey(nomPays + ";" + presidentFull)) duree = visitesPaysAfricains.get(nomPays + ";" + presidentFull);
                    System.out.println("        <pays nom=\"" + nomPays + "\" durée=\"" + ((duree==0)?"0":("P"+duree+"D")) + "\" " + attrFrancophone + "/>");
                });
                System.out.println("    </président>");
            });
            System.out.println("</liste-présidents>");
        } catch (JDOMException | IOException e) {
            e.printStackTrace();
        }
    }
}