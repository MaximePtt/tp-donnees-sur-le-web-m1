### Instructions à suivre pour afficher le résultat du traitement JDOM sans XPath dans un terminal à partir du fichier d'entrée :
- Ouvrir le répertoire Question2/DOM/WithoutXPath dans un terminal
- Lancer la transformation en faisant la commande `./startDOMWithoutXPath_Ex2.sh`