<?php
//Lecture fichier tp.xml
	$doc = new DOMDocument();
	$doc->validateOnParse = true;
	$doc->preserveWhiteSpace = false;
  $doc->load('tp.xml');

//Création fichier RésultatDomAvecXpath.xml
  $dom = new DOMDocument();
  $dom->encoding = 'utf-8';
  $dom->xmlVersion = '1.0';
  $dom->formatOutput = true;
  $xml_file_name = 'ResultatDomAvecXpath.xml';
  $xpath = new DOMXPath($doc);
  $racine = $dom->createElement("liste-présidents");
  $dom->appendChild($racine);

//requête Xpath pour récupérer tous les présidents de la république
  $presidents = $xpath->query("/déplacements/liste-personnes/personne[fonction/@type = 'Président de la République']");
//Création Node Président 
	foreach ($presidents as $president) {
		$NodePrésident = $dom->createElement("président");

// Ajout de l'attribut Nom à l'element Président
$PrésidentName = new DOMAttr("nom",$president->getAttribute("nom"));
$NodePrésident->setAttributeNode($PrésidentName);
//requête Xpath pour récupérer tous les pays africaains 
$PaysAfricains = $xpath->query("/déplacements/liste-pays/pays[encompassed/@continent='africa']");
//récupération de l'attribut xml:id du président 
$fonction = $president->childNodes[0]->getAttribute('xml:id');
//création de Noeud Pays
    foreach ($PaysAfricains as $pays) {
	     $NodePays = $dom->createElement("pays");
//Récupération de l'attribut xml:id de pays 
		$idPays = $pays->getAttribute('xml:id');
//Requête Xpath pour récupérer toutes les visites des présidents de la république dans des pays africains 
$visites = $xpath->query("/déplacements/liste-visites/visite[@pays = '".$idPays."' and @personne = '".$fonction."']");
$DuréeTotale = 0;

	foreach ($visites as $visite) {
		$debutVisitePays = new DateTime($visite->getAttribute("debut"));
		$finVisitePays = new DateTime($visite->getAttribute("fin"));
		$Durée = $debutVisitePays->diff($finVisitePays)->format("%r%a");
//DuréeTotale est la somme des durées et nous ajoutons à 1 pour compter aussi le dernier jour de chaque visite 
        $DuréeTotale = $DuréeTotale + intval($Durée) +1;
		 }
			if ($DuréeTotale == 0) {
				$durée = "0";
			}else {
				$durée = "P".$DuréeTotale."D";
			}
//Ajout de l'attribut durée à l'element Pays 
$DuréeAttribut = new DOMAttr("durée",$durée);
$NodePays->setAttributeNode($DuréeAttribut);
//Requête XPath pour récupérer les langues des pays africains visités par les président 
$Langues = $xpath->query("id('".$idPays."')/language");
		foreach ($Langues as $language) {
			if ($language->nodeValue == "French" && (!$language->hasAttribute("percentage")) ) {
			$francophone = "Officiel";
//Ajout de l'attribut francophone avec ça valeur officiel si il y'a parmis les langues le Français mais sans pourcentage
			$FrancophonePays = new DOMAttr("francophone", $francophone);
			$NodePays->setAttributeNode($FrancophonePays);
			} else{
		    if($language->nodeValue == "French" && floatval($language->getAttribute("percentage")) >= 30)
			{
			$francophone = "En-Partie";
//Ajout de l'attribut francophone avec ça valeur En-partie si il y'a parmis les langues le Français avec un pourcentage supérieur à 30
			$FrancophonePays = new DOMAttr("francophone", $francophone);
			$NodePays->setAttributeNode($FrancophonePays);
			}
		}
	}
					
//Ajouter l'attribut nom à l'element pays 
$NomPaysAttribut = new DOMAttr("nom",$pays->getAttribute("nom"));
$NodePays->setAttributeNode($NomPaysAttribut);
//Ajout de l'element pays à l'element président pour construire la bonne arbre XML
$NodePrésident->appendChild($NodePays);
}
$racine->appendChild($NodePrésident);
}
$dom->save($xml_file_name);

?>