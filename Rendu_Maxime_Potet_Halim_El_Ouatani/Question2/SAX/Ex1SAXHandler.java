import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class Ex1SAXHandler extends DefaultHandler {
    boolean isLanguage = false; // Flag contenant l'information de si l'on est dans une balise "language" ou non

    String lastPays; // Nom du dernier pays parcouru
    String lastPersonne; // Nom de la dernière personne parcourue
    Double lastPercentageLanguage; // Valeur en % du dernier language parcouru (-1 si non défini)
    
    ArrayList<String> paysAfricains = new ArrayList<>(); // Liste de nom_pays_africain
    HashMap<String, String> presidents = new HashMap<>(); // Tableau associatif [Clé: nom_président, Val: fonction_id]
    HashMap<String, Integer> visitesPaysAfricains = new HashMap<>(); // Tableau associatif [Clé: nom_pays_africain;fonction_id, Val: nombre_visites]
    HashMap<String, String> paysFrLanguage = new HashMap<>(); // Tableau associatif [Clé: nom_pays_africain, Val: texte_francophone]

    @Override
    public void startDocument() throws SAXException {
    }

    @Override
    public void endDocument() throws SAXException {
        /*
            Affichage du contenu du fichier de sortie
        */
        System.out.println(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                        "<!DOCTYPE liste-présidents\n" +
                        "  SYSTEM \"res.dtd\">\n" +
                        "<liste-présidents>"
        );
        presidents.forEach((presidentName, presidentFull) -> {
            System.out.println("    <président nom=\"" + presidentName + "\">");
            paysAfricains.forEach((nomPays) -> {
                int duree = 0;
                String francophoneString = "" + paysFrLanguage.get(nomPays);
                String attrFrancophone = "";
                if(!francophoneString.isEmpty() && !francophoneString.equals("null")) attrFrancophone = "franchophone=\"" + francophoneString + "\"";
                if(visitesPaysAfricains.containsKey(nomPays + ";" + presidentFull)) duree = visitesPaysAfricains.get(nomPays + ";" + presidentFull);
                System.out.println("        <pays nom=\"" + nomPays + "\" durée=\"" + ((duree==0)?"0":("P"+duree+"D")) + "\" " + attrFrancophone + "/>");
            });
            System.out.println("    </président>");
        });
        System.out.println("</liste-présidents>");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        switch(qName){
            /*
                Si une balise pays s'ouvre, alors :
                On enregistre le nom du pays dans la variable tampon lastPays
            */
            case "pays":
                lastPays = attributes.getValue("nom");
                break;
            /*
                Si une balise language s'ouvre, alors :
                On enregistre la valeur du pourcentage dans la variable tampon lastpercentageLanguage (-1 si la balise n'a pas de pourcentage),
                On met le flag isLangage indiquant si l'on est dans une balise language à true
            */
            case "language":
                isLanguage = true;
                try{
                    lastPercentageLanguage = Double.valueOf(attributes.getValue("percentage"));
                }
                catch (Exception e){
                    lastPercentageLanguage = -1.0;
                }
                break;
            /*
                Si une balise encompassed s'ouvre et l'attribut "encompassed" a pour valeur "africa", alors :
                On enregistre la valeur du dernier pays parcouru dans la liste paysAfricains
            */
            case "encompassed":
                if(attributes.getValue("continent").equals("africa")) paysAfricains.add(lastPays);
                break;
            /*
                Si une balise visite s'ouvre et paysAfricain et la liste paysAfricains contient la valeur de l'attribut "pays", alors :
                On enregistre le pays, l'id de la fonction du visiteur et la durée totale de visite dans la liste visitesPaysAfricains
            */
            case "visite":
                String pays = attributes.getValue("pays");
                if(paysAfricains.contains(pays)){
                    String personne = attributes.getValue("personne");
                    DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                    LocalDate dateDebut = LocalDate.parse(attributes.getValue("debut"), df);
                    LocalDate dateFin = LocalDate.parse(attributes.getValue("fin"), df);
                    int duree = (int) ChronoUnit.DAYS.between(dateDebut, dateFin) + 1;
                    if(visitesPaysAfricains.containsKey(pays + ";" + personne)){
                        int lastduree = visitesPaysAfricains.get(pays + ";" + personne);
                        visitesPaysAfricains.put(pays + ";" + personne, lastduree + duree);
                    }
                    else{
                        visitesPaysAfricains.put(pays + ";" + personne, duree);
                    }
                }
                break;
            /*
                Si une balise personne s'ouvre, alors :
                On enregistre le nom de la personne dans la variable tampon lastPersonne
            */
            case "personne":
                this.lastPersonne = attributes.getValue("nom");
                break;
            /*
                Si une balise fonction s'ouvre et l'attribut "type" a pour valeur "Président de la République", alors :
                On ajoute l'id de cette fonction dans la liste presidents
            */
            case "fonction":
                String type = attributes.getValue("type");
                if(type.equals("Président de la République")){
                    String presidentFull = attributes.getValue("xml:id");
                    presidents.put(this.lastPersonne, presidentFull);
                }
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch(qName){
            /*
                Si une balise language se ferme, alors :
                On met le flag isLangage indiquant si l'on est dans une balise language à false
            */
            case "language":
                isLanguage = false;
                break;
        }
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        /*
            Si un noeud texte dans une balise "language" est parcouru ET que sa valeur est "French", alors :
            On enregistre dans paysFrLanguage le nom du pays et la façon dont le français est parlé (en-partie, officiellement, ou non)
        */
        String text = new String(ch, start, length);
        if(isLanguage && text.equals("French")){
            if(lastPercentageLanguage >= 30){
                paysFrLanguage.put(lastPays, "En-partie");
            }
            else if(lastPercentageLanguage == -1.0){
                paysFrLanguage.put(lastPays, "Officiel");
            }
            else{
                paysFrLanguage.put(lastPays, "");
            }
        }
    }
}
