import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;

public class SAXParserEx1 {
    public static void main(String[] args){
        try {
            //File Path
            String filePath = "./tp.xml";

            //Create file object.
            File inputFile = new File(filePath);

            //Get SAXParserFactory instance.
            SAXParserFactory factory=SAXParserFactory.newInstance();

            //Get SAXParser object from SAXParserFactory instance.
            SAXParser saxParser = factory.newSAXParser();

            //Create StudentHandler object.
            Ex1SAXHandler studentHandler = new Ex1SAXHandler();

            //Parse the XML file.
            saxParser.parse(inputFile, studentHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
