xquery version "1.0";
declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "xml";
declare option output:media-type "text/xml";
declare option output:omit-xml-declaration "no";
declare option output:indent "yes";
declare option output:doctype-system "tp.dtd";



<liste-president> 
{
(: Récupération des  présidents de la république  :)  

    for $president  in doc("tp.xml")/déplacements/liste-personnes/personne[./fonction/@type =  'Président de la République']
    (:création de l'element président et récupération de l'attribut nom:)
      return <president>{$president/@nom}
     
      {
        (: Récupération des  pays africains  :)  

      for  $pays  in doc ("tp.xml")/déplacements/liste-pays/pays[ ./encompassed/@continent = 'africa']
        (:Durée de visites du président aux pays africains :)
         let $duree := doc("tp.xml")/sum(/déplacements/liste-visites/visite[./@personne= $president/fonction/@xml:id and  @pays =$pays/@xml:id]/days-from-duration(xs:date(@fin) - xs:date(@debut)))
       (:Nombre de visites du présient aux pays africains :)
  let $visite := doc ("tp.xml")/count(/déplacements/liste-visites/visite[./@personne= $president/fonction/@xml:id and  @pays =$pays/@xml:id])
          let $dureeTotale := $duree + $visite 
         
          
       
          
          
          return    
      
         if ($pays/language[contains(.,'French') and  (@percentage >=30  or (not(@percentage)))])
                     then (
                      (:nous testons s'il existe une langue Français mais sans aucun pourcentage alors l'attribut francophone est officiel:)
                          if( $pays/language[(not(@percentage) ) and contains(.,'French')] ) then  (
                           <pays  nom='{$pays/@nom}'   
                          
                                 duree='{ $dureeTotale}'

                                 francophone="officiel" >
                                </pays>
                            )
            (:nous testons s'il existe une langue Français avec un pourcentage supérieur ou égal au 30 alors l'attribut francophone est en-partie :)

                          else (
                            <pays  nom='{$pays/@nom}'   
                             duree="{$dureeTotale}"
                                 francophone="en-partie" >
                                </pays>
  
                            )
                           )  
    (:Sinon nous n'avons pas d'attribut francophone:)

                     else (
                         <pays  nom='{$pays/@nom}'   
                             duree="{$dureeTotale}" >
                        </pays>

                     )


               
 

          
         
      }
      
       </president>
    
}
</liste-president>

